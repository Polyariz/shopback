﻿using ShopBack.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace ShopBack.Controllers.API
{
    public class ShopItemsController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/ShopItems
        public IQueryable<ShopItem> GetShopItems()
        {
            return db.ShopItems;
        }

        // GET: api/ShopItems/5
        [ResponseType(typeof(ShopItem))]
        public IHttpActionResult GetShopItem(int id)
        {
            ShopItem shopItem = db.ShopItems.Find(id);
            if (shopItem == null)
            {
                return NotFound();
            }

            return Ok(shopItem);
        }

        // PUT: api/ShopItems/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutShopItem(int id, ShopItem shopItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != shopItem.ShopItemId)
            {
                return BadRequest();
            }

            db.Entry(shopItem).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShopItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ShopItems
        [ResponseType(typeof(ShopItem))]
        public IHttpActionResult PostShopItem(ShopItem shopItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ShopItems.Add(shopItem);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = shopItem.ShopItemId }, shopItem);
        }

        // DELETE: api/ShopItems/5
        [ResponseType(typeof(ShopItem))]
        public IHttpActionResult DeleteShopItem(int id)
        {
            ShopItem shopItem = db.ShopItems.Find(id);
            if (shopItem == null)
            {
                return NotFound();
            }

            db.ShopItems.Remove(shopItem);
            db.SaveChanges();

            return Ok(shopItem);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShopItemExists(int id)
        {
            return db.ShopItems.Count(e => e.ShopItemId == id) > 0;
        }
    }
}