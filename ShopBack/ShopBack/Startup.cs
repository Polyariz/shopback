﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ShopBack.Startup))]
namespace ShopBack
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
