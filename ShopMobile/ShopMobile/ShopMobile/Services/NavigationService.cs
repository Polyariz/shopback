﻿using ShopMobile.Models;
using ShopMobile.Pages;
using ShopMobile.ViewModels;
using System.Threading.Tasks;

namespace ShopMobile.Services
{
    public class NavigationService
    {
        public async Task Navigate(string pageName)
        {
            var mainViewModel = MainViewModel.GetInstance();

            switch (pageName)
            {
                case nameof(NewShopItemPage):
                    mainViewModel.NewShopItem = new NewShopItemViewModel();
                    await App.Current.MainPage.Navigation.PushAsync(new NewShopItemPage());
                    break;
                default:
                    break;
            }
        }

        public async Task Back()
        {
            await App.Current.MainPage.Navigation.PopAsync();
        }
        public async Task EditItem(ShopItem item)
        {
            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.EditShopItem = new EditShopItemViewModel(item);
            await App.Current.MainPage.Navigation.PushAsync(new EditShopItemPage());
        }

    }
}
