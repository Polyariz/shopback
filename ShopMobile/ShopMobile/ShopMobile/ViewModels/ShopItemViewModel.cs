﻿using GalaSoft.MvvmLight.Command;
using ShopMobile.Models;
using ShopMobile.Services;
using System.Windows.Input;

namespace ShopMobile.ViewModels
{
    public class ShopItemViewModel : ShopItem
    {
        #region Attributes

        private NavigationService navigationService;

        #endregion

        #region Constructors

        public ShopItemViewModel()
        {
            navigationService = new NavigationService();
        }

        #endregion

        #region Commands

        public ICommand EditShopItemCommand { get { return new RelayCommand(EditShopItem); } } 

        private async void EditShopItem()
        {
            await navigationService.EditItem(this);
        }

        #endregion

    }
}
