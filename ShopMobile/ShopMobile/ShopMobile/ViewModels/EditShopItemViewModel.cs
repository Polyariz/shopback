﻿using GalaSoft.MvvmLight.Command;
using ShopMobile.Models;
using ShopMobile.Services;
using System.ComponentModel;
using System.Windows.Input;

namespace ShopMobile.ViewModels
{
    public class EditShopItemViewModel : ShopItem, INotifyPropertyChanged
    {

        #region Attributes

        private ApiService apiService;

        private DialogService dialogService;

        private NavigationService navigationService;

        private string descrpition;

        private decimal price;

        private bool isRunning;

        private bool isEnabled;

        #endregion

        #region Properties

        public new string Description
        {
            set
            {
                if (descrpition != value)
                {
                    descrpition = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Description)));
                }
            }
            get
            {
                return descrpition;
            }
        }

        public new decimal Price
        {
            set
            {
                if (price != value)
                {
                    price = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Price)));
                }
            }
            get
            {
                return price;
            }
        }

        public bool IsRunning
        {
            set
            {
                if (isRunning != value)
                {
                    isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsRunning)));
                }
            }
            get
            {
                return isRunning;
            }
        }

        public bool IsEnabled
        {
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsEnabled)));
                }
            }
            get
            {
                return isEnabled;
            }
        }

        #endregion

        #region Constructors

        public EditShopItemViewModel(ShopItem item)
        {
            Description = item.Description;
            ShopItemId = item.ShopItemId;
            Price = item.Price;

            apiService = new ApiService();
            dialogService = new DialogService();
            navigationService = new NavigationService();

            IsEnabled = true;
        }

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Commands

        public ICommand SaveItemCommand { get { return new RelayCommand(SaveItem); } }

        private async void SaveItem()
        {
            if (string.IsNullOrEmpty(Description))
            {
                await dialogService.ShowMessage("Error", "You must enter a description");
                return;
            }

            if (Price <= 0)
            {
                await dialogService.ShowMessage("Error", "The price must be greather than zero");
                return;
            }

            IsRunning = true;
            IsEnabled = false;

            var item = new ShopItem
            {
                Description = Description,
                ShopItemId = ShopItemId,
                Price = Price,
            };

            var result = await apiService.Put("https://myshopback.azurewebsites.net/", "/api", "/ShopItems", item);

            IsRunning = false;
            IsEnabled = true;

            if (!result.IsSuccess)
            {
                await dialogService.ShowMessage("Error", result.Message);
                return;
            }

            Description = string.Empty;
            Price = 0;
            await navigationService.Back();
        }

        public ICommand DeleteItemCommand { get { return new RelayCommand(DeleteItem); } }

        private async void DeleteItem()
        {
            var answer = await dialogService.ShowConfirm("Confirm", "Are you sure to delete this record?");
            if (!answer)
            {
                return;
            }

            IsRunning = true;
            IsEnabled = false;

            var item = new ShopItem
            {
                Description = Description,
                ShopItemId = ShopItemId,
                Price = Price,
            };

            var result = await apiService.Delete("https://myshopback.azurewebsites.net/", "/api", "/ShopItems", item);

            IsRunning = false;
            IsEnabled = true;

            if (!result.IsSuccess)
            {
                await dialogService.ShowMessage("Error", result.Message);
                return;
            }

            Description = string.Empty;
            Price = 0;
            await navigationService.Back();
        }

        #endregion
    }
}
