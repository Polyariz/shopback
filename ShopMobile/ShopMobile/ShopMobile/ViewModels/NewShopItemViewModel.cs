﻿using GalaSoft.MvvmLight.Command;
using ShopMobile.Models;
using ShopMobile.Services;
using System.ComponentModel;
using System.Windows.Input;

namespace ShopMobile.ViewModels
{
    public class NewShopItemViewModel : INotifyPropertyChanged
    {
        #region Attributes
        private ApiService apiService;
        private NavigationService navigationService;
        private DialogService dialogService;
        private string descrpition;

        private decimal price;
        private bool isRunning;
        private bool isEnabled;

        #endregion
        #region Properties
        public string Descrpition
        {
            set
            {
                if (descrpition != value)
                {
                    descrpition = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Descrpition)));
                }
            }
            get
            {
                return descrpition;
            }
        }

        public decimal Price
        {
            set
            {
                if (price != value)
                {
                    price = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Price)));
                }
            }
            get
            {
                return price;
            }
        }

        public bool IsRunning
        {
            set
            {
                if (isRunning != value)
                {
                    isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsRunning)));
                }
            }
            get
            {
                return isRunning;
            }
        }

        public bool IsEnabled
        {
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsEnabled)));
                }
            }
            get
            {
                return isEnabled;
            }
        }

        #endregion

        #region Constructors
        public NewShopItemViewModel()
        {
            apiService = new ApiService();
            navigationService = new NavigationService();

            dialogService = new DialogService();
            IsEnabled = true;

        }
        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Commands
        public ICommand NewItemCommand { get { return new RelayCommand(NewItem); } } 

        private async void NewItem()
        {
            if (string.IsNullOrEmpty(Descrpition))
            {
                await dialogService.ShowMessage("Error", "You must enter a description");
                return;
            }

            if (Price <= 0)
            {
                await dialogService.ShowMessage("Error", "The price must be greather than zero");
                return;
            }

            var item = new ShopItem
            {
                Description = Descrpition,
                Price = Price
            };

            IsRunning = true;
            IsEnabled = false;

            var response = await apiService.Post<ShopItem>("https://myshopback.azurewebsites.net/", "/api", "/ShopItems", item);

            IsRunning = false;
            IsEnabled = true;

            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage("Error", response.Message);
                return;
            }

            await navigationService.Back();
        }

        #endregion
    }
}
