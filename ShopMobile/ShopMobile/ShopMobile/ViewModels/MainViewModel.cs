﻿using GalaSoft.MvvmLight.Command;
using ShopMobile.Models;
using ShopMobile.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace ShopMobile.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        #region Attributes        
        private ApiService apiService;
        private NavigationService navigationService;
        private DialogService dialogService;
        private bool isRefreshing;
        #endregion
        #region Properties

        public ObservableCollection<ShopItemViewModel> ShopItems { get; set; }
        public NewShopItemViewModel NewShopItem { get; set; }
        public EditShopItemViewModel EditShopItem { get; set; }
        public bool IsRefreshing
        {
            set
            {
                if (isRefreshing != value)
                {
                    isRefreshing = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsRefreshing)));
                }
            }
            get
            {
                return isRefreshing;
            }
        }

        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Constructors
        public MainViewModel()
        {
            //Singlton
            instance = this;

            //Services
            apiService = new ApiService();
            navigationService = new NavigationService();
            dialogService = new DialogService();

            //ViewModels
            ShopItems = new ObservableCollection<ShopItemViewModel>();
            NewShopItem = new NewShopItemViewModel(); 
        }

        private async void LoadShopItems()
        { 
            var response = await apiService.Get<ShopItem>("https://myshopback.azurewebsites.net/", "/api", "/ShopItems");

            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage("Error", response.Message);
                return;
            }
            ReloadShopItems((List<ShopItem>)response.Result);
        }

        private void ReloadShopItems(List<ShopItem> result) 
        {
            ShopItems.Clear();
            foreach (var item in result.OrderBy(f => f.Description))
            {
                ShopItems.Add(new ShopItemViewModel
                {
                    Description = item.Description,
                    ShopItemId = item.ShopItemId,
                    Price = item.Price
                });
            }
        }
        #endregion

        #region Methods

        #endregion

        #region Commands
        public ICommand RefreshShopItemCommand { get { return new RelayCommand(RefreshShopItem); } }
        private void RefreshShopItem()
        {
            IsRefreshing = true;
            LoadShopItems();
            IsRefreshing = false;
        }

        public ICommand AddShopItemCommand { get { return new RelayCommand(AddShopItem); } }

        private async void AddShopItem()
        {
            await navigationService.Navigate(nameof(Pages.NewShopItemPage));
        }
        #endregion

        #region Singleton

        private static MainViewModel instance;
        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                instance = new MainViewModel();
            }

            return instance;
        }

        #endregion

    }
}
