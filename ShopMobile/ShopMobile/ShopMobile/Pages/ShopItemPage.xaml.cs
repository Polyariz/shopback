﻿using ShopMobile.ViewModels;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShopMobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShopItemPage : ContentPage
	{
		public ShopItemPage ()
		{
			InitializeComponent ();

            var mainViewModel = MainViewModel.GetInstance();
            base.Appearing += (object sender, EventArgs e) =>
            {
                mainViewModel.RefreshShopItemCommand.Execute(this);
            };
        }
	}
}