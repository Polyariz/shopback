﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShopMobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewShopItemPage : ContentPage
	{
		public NewShopItemPage ()
		{
			InitializeComponent ();
		}
	}
}