﻿using ShopMobile.ViewModels;

namespace ShopMobile.Infrastructure
{
    public class InstanceLocator
    {
        public MainViewModel Main { get; private set; }

        public InstanceLocator()
        {
            Main = new MainViewModel();
        }
    }
}
