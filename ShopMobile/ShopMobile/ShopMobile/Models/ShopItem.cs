﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopMobile.Models
{ 
    public class ShopItem
    {
        public int ShopItemId { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }

        public override int GetHashCode()
        {
            return ShopItemId;
        }
    }
}
